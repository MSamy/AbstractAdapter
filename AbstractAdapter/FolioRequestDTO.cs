﻿namespace AbstractAdapter
{
    public class FolioRequestDTO: CambridgeDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}