﻿namespace AbstractAdapter
{
    public class CLICRequestDTO: CambridgeDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}