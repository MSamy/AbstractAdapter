﻿namespace AbstractAdapter
{
    public class FolioAdapter: AbstractAdapter
    {
        public override CambridgeDTO Read(CambridgeDTO dto)
        {
            return base.Read(dto);
        }

        public override CambridgeDTO Write(CambridgeDTO dto)
        {
            return base.Write(dto);
        }
    }
}