﻿using System;

namespace AbstractAdapter
{
    public abstract class AbstractAdapter: IAdapter
    {
        public virtual CambridgeDTO Read(CambridgeDTO dto)
        {
            Console.WriteLine("Abstract Read");
            throw new System.NotImplementedException();
        }

        public virtual CambridgeDTO Write(CambridgeDTO dto)
        {
            Console.WriteLine("Abstract Write");
            throw new System.NotImplementedException();
        }
    }
}