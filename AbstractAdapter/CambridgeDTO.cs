﻿using System;

namespace AbstractAdapter
{
    public class CambridgeDTO
    {
        Guid domGuid = new Guid();
        public Guid DomainID
        {
            get { return domGuid; }
        }
    }
}