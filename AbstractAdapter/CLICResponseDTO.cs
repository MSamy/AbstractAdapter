﻿namespace AbstractAdapter
{
    public class CLICResponseDTO :CambridgeDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}