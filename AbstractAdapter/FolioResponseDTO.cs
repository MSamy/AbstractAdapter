﻿namespace AbstractAdapter
{
    public class FolioResponseDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}