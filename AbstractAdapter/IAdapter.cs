﻿namespace AbstractAdapter
{
    public interface IAdapter
    {
        CambridgeDTO Read(CambridgeDTO dto);
        CambridgeDTO Write(CambridgeDTO dto);
    }

}

    

    